#!/bin/bash

# launch this script in the "course/" folder to rename the whole file structure
# according to the mooc order
# TODO: use meaningful filenames

cha=0
for chapter in `cat course/2021_1.xml | grep chapter | cut -d'"' -f2`; do 
  ((cha=cha+1))
  printf -v chap "%02d" $cha
  newchap=chap-$chap
  se=0
  for sequential in `cat chapter/$chapter.xml | grep sequential | cut -d'"' -f2`; do
    ((se=se+1))
    printf -v seq "%02d" $se
    newseq=chap-$chap-seq-$seq
    ve=0
    for vertical in `cat sequential/$sequential.xml | grep vertical | cut -d'"' -f2`; do
      ((ve=ve+1))
      printf -v ver "%02d" $ve
      newver=chap-$chap-seq-$seq-ver-$ver
      h=0
      for html in `cat vertical/$vertical.xml | grep '<html url' | cut -d'"' -f2`; do
        ((h=h+1))
        printf -v ht "%02d" $h
        newhtml=chap-$chap-seq-$seq-ver-$ver-html-$ht
        sed -i 's/'$html'/'$newhtml'/' vertical/$vertical.xml
        sed -i 's/'$html'/'$newhtml'/' html/$html.xml
        git mv html/$html.xml html/$newhtml.xml
        git mv html/$html.html html/$newhtml.html
      done
      sed -i 's/'$vertical'/'$newver'/' sequential/$sequential.xml
      git mv vertical/$vertical.xml vertical/$newver.xml
    done
    sed -i 's/'$sequential'/'$newseq'/' chapter/$chapter.xml
    git mv sequential/$sequential.xml sequential/$newseq.xml
  done
  sed -i 's/'$chapter'/'$newchap'/' course/2021_1.xml
  git mv chapter/$chapter.xml chapter/$newchap.xml
done

#!/bin/bash

# This script can be used to modify course export files that were exported from a course
# with a different org/course/run combination than the one that we are currently enforcing.
# The script should be run from the repository root with `./scripts/rename-course.sh`.

DST_ORG="MOOC-FLOSS"
DST_COURSE="101"
DST_RUN="2021_1"

SRC_ORG="$(grep -ho 'org=\"[^\"]*\"' course/course.xml | cut -d \" -f 2)"
SRC_COURSE="$(grep -ho 'course=\"[^\"]*\"' course/course.xml | cut -d \" -f 2)"
SRC_RUN="$(grep -ho 'url_name=\"[^\"]*\"' course/course.xml | cut -d \" -f 2)"

OS=`uname`

function replace_inline() {
    if [ "$OS" = 'Darwin' ]; then
        sed -i '' -e "$1" "$2"
    else
        sed -i'' -e "$1" "$2"
    fi
}

echo "This script will rename your course from $SRC_ORG/$SRC_COURSE/$SRC_RUN to $DST_ORG/$DST_COURSE/$DST_RUN"
echo "It will modify existing files in-place."
read -p "Are you sure you want to continue? [y/n] "

if [[ ! $REPLY =~ ^[Yy]$ ]]; then
    echo "You are not sure; aborting..."
    exit 1
fi

if [ -f "course/course/$SRC_RUN.xml" ]; then
    echo "Moving course/course/$SRC_RUN.xml to course/course/$DST_RUN.xml ..."
    mv -f "course/course/$SRC_RUN.xml" "course/course/$DST_RUN.xml"
fi

if [ -d "course/policies/$SRC_RUN" ]; then
    if [ -f "course/policies/$SRC_RUN/policy.json" ]; then
        echo "Renaming references in policy.json ..."
        replace_inline 's/"course\/'"$SRC_RUN"'"/"course\/'"$DST_RUN"'"/g' "course/policies/$SRC_RUN/policy.json"
        replace_inline 's/"course\/'"$SRC_RUN"'.xml"/"course\/'"$DST_RUN"'.xml"/g' "course/policies/$SRC_RUN/policy.json"
    fi
    echo "Moving course/policies/$SRC_RUN/* to course/policies/$DST_RUN/ ..."
    mkdir -p "course/policies/$DST_RUN"
    find "course/policies/$SRC_RUN" -type f -exec mv '{}' "course/policies/$DST_RUN" \;
fi

echo "Renaming references in assets.json ..."
replace_inline 's/"org": "'"$SRC_ORG"'"/"org": "'"$DST_ORG"'"/g' course/policies/assets.json
replace_inline 's/"course": "'"$SRC_COURSE"'"/"course": "'"$DST_COURSE"'"/g' course/policies/assets.json
replace_inline 's/"run": "'"$SRC_RUN"'"/"run": "'"$DST_RUN"'"/g' course/policies/assets.json
replace_inline "s/asset-v1:$SRC_ORG+$SRC_COURSE+$SRC_RUN/asset-v1:$DST_ORG+$DST_COURSE+$DST_RUN/g" course/policies/assets.json

echo "Renaming wiki slug in $DST_RUN.xml ..."
replace_inline 's/slug="'"$SRC_ORG\.$SRC_COURSE\.$SRC_RUN"'"/slug="'"$DST_ORG.$DST_COURSE.$DST_RUN"'"/g' "course/course/$DST_RUN.xml"

echo "Renaming references in course.xml ..."
replace_inline 's/org="'"$SRC_ORG"'"/org="'"$DST_ORG"'"/g' course/course.xml
replace_inline 's/course="'"$SRC_COURSE"'"/course="'"$DST_COURSE"'"/g' course/course.xml
replace_inline 's/url_name="'"$SRC_RUN"'"/url_name="'"$DST_RUN"'"/g' course/course.xml

echo "Done"
